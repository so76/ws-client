import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import jwt_decode from 'jwt-decode';
import { HttpClient } from '@angular/common/http';
import { ICourse, ICourses, ILogin, IScore, ScoreAndStudent } from '../core/interface/interface';
@Injectable({
  providedIn: 'root',
})
export class AuthService {

  idCourse!: string;
  constructor(private http: HttpClient) {}

  decryptSession(dataEncrypt: string) {
    const decode = jwt_decode(dataEncrypt);
    return decode;
  }

  onLogin(body: ILogin) {
    return this.http.post(`${environment.ApiEndpoint}/login`, body);
  }

  onGetCourses() {
    return this.http.get<ICourses[]>(`${environment.ApiEndpoint}/courses`);
  }

  onGetCourse(id: string){
    return this.http.get<ICourse>(`${environment.ApiEndpoint}/courses/${id}`);
  }

  onGetScoreAndStudent(id: string){
    return this.http.get<ScoreAndStudent[]>(`${environment.ApiEndpoint}/scores/course/${id}`);
  }

  onUpdateCourse(body: IScore, id: string ){
    return this.http.patch(`${environment.ApiEndpoint}/scores/${id}`, body);
  }

}

