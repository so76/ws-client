import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DefaultLayoutComponent } from './containers';


const routes: Routes = [
  {
    path: 'acceso',
    loadChildren:() => 
      import('./views/pages/pages.module').then((m) => m.PagesModule),
  },
  {
    path: 'colegio',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./views/dashboard/dashboard.module').then((m) => m.DashboardModule)
      },
      {
        path: 'cursos',
        loadChildren: () =>
          import('./views/card-course/card-course.module').then((m) => m.CardCourseModule)
      },
      {
        path: 'registro-nota/:name',
        loadChildren: () =>
          import('./views/notes-record/notes-record.module').then((m) => m.NotesRecordModule)
      }
    ]
  },

  {
    path: '',
    redirectTo: 'acceso',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'top',
      anchorScrolling: 'enabled',
      initialNavigation: 'enabledBlocking'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
