import { LogoutComponent } from './../../../views/dialogs/logout/logout.component';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { ClassToggleService, HeaderComponent } from '@coreui/angular';
import { DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-default-header',
  templateUrl: './default-header.component.html',
})
export class DefaultHeaderComponent extends HeaderComponent implements OnInit {
  @Input() sidebarId: string = 'sidebar';

  public newMessages = new Array(4);
  public newTasks = new Array(5);
  public newNotifications = new Array(5);
  positionRole!: string;
  fullName!: string;
  dataStorage: any;
  constructor(
    private classToggler: ClassToggleService,
    private dialogService: DialogService
  ) {
    super();
  }
  textSendSucces: string = '¿Desea cerrar sesión?';

  ngOnInit() {
    this.dataStorage = JSON.parse(localStorage.getItem('user') || '{}');
    this.positionRole = this.dataStorage.role;
    this.fullName = this.dataStorage.fullname
  }

  open() {
    this.dialogService.open(LogoutComponent, {
      data: this.textSendSucces,
      showHeader: false,
      width: '35%',
      dismissableMask: true,
      contentStyle: { 'border-radius': '15px' },
      baseZIndex: 10000,
    });
  }
}
