export interface ILogin {
  email: string;
  password: string;
}

export interface ICourses {
  _id: string;
  code: number;
  name: string;
  image: string;
}

export interface ICourse {
  _id: string;
  code: number;
  name: string;
  teacher: IStatus;
  classroom: IClassroom;
  status: IStatus;
  image: string;
}

export interface IClassroom {
  _id: string;
  code: string;
}

export interface IStatus {
  _id: string;
  name: string;
}

export interface ScoreAndStudent {
  _id: string;
  scores: Score[];
  student_course: StudentCourse;
}

export interface Score {
  name: string;
  score: number;
  date: Date;
}

export interface StudentCourse {
  _id: string;
  student: Student;
  course: Course;
}

export interface Course {
  _id: string;
  name: string;
}

export interface Student {
  _id: string;
  name: string;
  lastname: string;
}

export interface IScore {
  scores: Score[];
}

export interface Score {
  name: string;
  score: number;
}
