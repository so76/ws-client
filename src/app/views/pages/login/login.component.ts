import { Router } from '@angular/router';
import { AuthService } from './../../../services/auth.service';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  formLogin!: FormGroup;
  dataEncrypt: any;
  sesionLocalStorage: any;
  isHide: boolean = false;

  private specialKeys: Array<string> = ['Space', 'End', 'Home'];

  messageError: boolean = false;
  btnDisable: boolean = false;
  constructor(
    private _fb: FormBuilder,
    private _auth: AuthService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.initForms();
  }

  initForms() {
    this.formLogin = this._fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

  login() {
    const body = {
      email: this.formLogin.get('email')?.value,
      password: this.formLogin.get('password')?.value,
    };
    console.log(body);
    
    this._auth.onLogin(body).subscribe(
      (data) => {
        this.formLogin.disable();
        this.btnDisable = true;
        this.dataEncrypt = data;
        localStorage.setItem('token', this.dataEncrypt.access_token);
        // this.cookieService.set('token', this.dataEncrypt.access_token);
        this.sesionLocalStorage = this._auth.decryptSession(
          this.dataEncrypt.access_token
        );
        localStorage.setItem('user', JSON.stringify(this.sesionLocalStorage));
        if (data) {
          this._router.navigate(['../../colegio/dashboard']);
        }
      },
      (error) => {
        this.messageError = true;
        // this.formLogin.reset();
        setTimeout(() => {
          this.messageError = false;
        }, 3000);
      }
    );
  }
}
