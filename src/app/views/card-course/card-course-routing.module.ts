import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardCourseComponent } from './card-course.component';


const routes: Routes = [
  {
    path: '',
    component: CardCourseComponent,

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CardCourseRoutingModule {}

