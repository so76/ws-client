import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BadgeModule, CardModule, FormModule, GridModule } from '@coreui/angular';
import { ChartjsModule } from '@coreui/angular-chartjs';

import { CardCourseComponent } from './card-course.component';
// import { DocsComponentsModule } from '@docs-components/docs-components.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CardCourseRoutingModule } from './card-course-routing.module';

@NgModule({
  declarations: [CardCourseComponent],
  imports: [
    CommonModule,
    CardCourseRoutingModule,
    ChartjsModule,
    CardModule,
    GridModule,
    BadgeModule,
    // DocsComponentsModule,
    FormModule,
    ReactiveFormsModule
    
  ]
})
export class CardCourseModule {
}
