import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ICourses } from 'src/app/core/interface/interface';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-card-course',
  templateUrl: './card-course.component.html',
  styleUrls: ['./card-course.component.scss'],
})
export class CardCourseComponent {
  constructor(
    private _fb: FormBuilder,
    private _auth: AuthService,
    private _router: Router
  ) {}

  listCourses: ICourses[] = [];

  ngOnInit(): void {
    this.initForms();
    this.getCourses();
  }

  initForms() {}

  getCourses() {
    this._auth.onGetCourses().subscribe((data) => {
      this.listCourses = data;
      console.log(data);
    });
  }

  showCourse(idCourse: string){
    this._auth.idCourse = idCourse
    console.log(idCourse);
    this._router.navigate(['/colegio/registro-nota/'+idCourse]);
    
  }
}
