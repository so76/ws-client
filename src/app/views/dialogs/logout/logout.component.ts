import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  DialogService,
  DynamicDialogConfig,
  DynamicDialogRef,
} from 'primeng/dynamicdialog';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss'],
  providers: [DialogService],
})
export class LogoutComponent implements OnInit {
  message!: string;
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.message = this.config.data;
    console.log(this.message);
  }

  logoutSession() {
    localStorage.removeItem('user');
    this.ref.close();
    this._router.navigate(['../../acceso/login']);

  }

  cancel() {
    this.ref.close();
  }
}
