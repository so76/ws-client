import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SuccessfulComponent } from './successful/successful.component';
import { DialogService, DynamicDialogModule } from 'primeng/dynamicdialog';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  declarations: [SuccessfulComponent, LogoutComponent],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    DynamicDialogModule

  ],

  exports: [],
  providers: [DialogService],
})
export class SharedModule {}
