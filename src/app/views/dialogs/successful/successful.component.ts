import { Component, OnInit } from '@angular/core';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
@Component({
  selector: 'app-successful',
  templateUrl: './successful.component.html',
  styleUrls: ['./successful.component.scss'],
  providers: [DialogService],
})
export class SuccessfulComponent implements OnInit {
  message!: string;
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
  ) { }

  ngOnInit(): void {
    this.message = this.config.data;
    console.log(this.message);
  }
  closePOPUP(){
    this.ref.close();
  }

}


