import { ICourse, ScoreAndStudent } from '../../core/interface/interface';
import {
  AfterViewInit,
  Component,
  HostBinding,
  Inject,
  Input,
  OnInit,
  Renderer2,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { getStyle, rgbToHex } from '@coreui/utils/src';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormArray,
  AbstractControl,
} from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { DialogService } from 'primeng/dynamicdialog';
import { SuccessfulComponent } from '../dialogs/successful/successful.component';

@Component({
  templateUrl: 'notes-record.component.html',
})
export class NotesRecordComponent implements OnInit, AfterViewInit {
  idCourse!: string;
  disableBtn: boolean = true;
  descriptionCourse: ICourse | undefined;
  formRegistro!: FormGroup;
  dataScoreAndStudent: ScoreAndStudent[] = [];
  nameCourse!: string;
  codeCourse!: number;

  textSendSucces: string = 'Notas registradas correctamente';
  testSendError: string = 'Ocurrió un error al registrar';
  constructor(
    private _fb: FormBuilder,
    private _auth: AuthService,
    private _router: Router,
    private dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.initForms();
    this.idCourse = this._auth.idCourse;
    this.getCourse(this.idCourse);
    this.getScoreAndStudent(this.idCourse);
  }

  initForms() {
    this.formRegistro = this._fb.group({
      student: this._fb.array([]),
    });
  }

  getCourse(id: string) {
    this._auth.onGetCourse(id).subscribe((data) => {
      this.descriptionCourse = data;
      this.codeCourse = this.descriptionCourse.code;
      this.nameCourse = this.descriptionCourse.name;
      console.log(this.descriptionCourse);
    });
  }

  getScoreAndStudent(id: string) {
    this._auth.onGetScoreAndStudent(id).subscribe((data) => {
      console.log(data);
      this.dataScoreAndStudent = data;
      this.dataScoreAndStudent.forEach((elm) => {
        (this.formRegistro.get('student') as FormArray).push(
          this._fb.group({
            id: elm._id,
            name:
              elm.student_course.student.name +
              ' ' +
              elm.student_course.student.lastname,
            scores: this._fb.array(this.scoresTa(elm.scores)),
          })
        );
      });
    });
  }

  scoresTa(elx: any) {
    return elx.map((elm: any) => {
      return this._fb.group({ name: elm.name, score: Number(elm.score) });
    });
  }

  register(form: AbstractControl) {
    const scores = form.value.scores;
    const body = {
      scores,
    };
    // console.log(form.value);
    console.log(body);
    this._auth.onUpdateCourse(body, form.value.id).subscribe(
      (data) => {
        console.log(data);
        this.dialogService.open(SuccessfulComponent, {
          data: this.textSendSucces,
          showHeader: false,
          width: '35%',
          dismissableMask: true,
          contentStyle: { 'border-radius': '15px' },
          baseZIndex: 10000,
        });
      },
      (error) => {
        this.dialogService.open(SuccessfulComponent, {
          data: this.testSendError,
          showHeader: false,
          width: '35%',
          dismissableMask: true,
          contentStyle: { 'border-radius': '15px' },
          baseZIndex: 10000,
        });
      }
    );
  }

  get students() {
    return this.formRegistro.get('student') as FormArray;
  }

  getScores(form: AbstractControl) {
    return form.get('scores') as FormArray;
  }

  disabledButton() {
    // this.disableBtn = true;
    this.formRegistro.get('scores');
  }
  enableButton() {
    // this.disableBtn = false;
  }

  ngAfterViewInit(): void {}
}
