import { DocsComponentsModule } from '../../../components/docs-components.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CardModule, GridModule, NavModule, UtilitiesModule, TabsModule } from '@coreui/angular';
import { IconModule } from '@coreui/icons-angular';

import { NotesRecordComponent } from './notes-record.component';
// import { TypographyComponent } from './typography.component';

// Theme Routing
import { ReactiveFormsModule } from '@angular/forms';
import { OnlyNumberDirective } from 'src/app/core/directives/only-number.directive';
import { DirectivesModule } from 'src/app/core/directives/directives.module';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { SharedModule } from '../dialogs/dialogs.module';
import { NotesRecordRoutingModule } from './notes-record-routing.module';

@NgModule({
  imports: [
    CommonModule,
    NotesRecordRoutingModule,
    CardModule,
    GridModule,
    UtilitiesModule,
    IconModule,
    NavModule,
    TabsModule,
    DocsComponentsModule,
    ReactiveFormsModule,
    DirectivesModule,
    SharedModule,
    DynamicDialogModule
  ],
  declarations: [
    NotesRecordComponent,

  ]
})
export class NotesRecordModule {
  
}
