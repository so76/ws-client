import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotesRecordComponent } from './notes-record.component';
// import { TypographyComponent } from './typography.component';

const routes: Routes = [
  {
    path: '',
    component: NotesRecordComponent

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotesRecordRoutingModule {}
